import test from 'ava';
import { getSubsections } from 'util/sections';

test('returns an empty array when missing args', (t) => {
    t.plan(3);
    t.deepEqual(getSubsections(), []);
    t.deepEqual(getSubsections('asd'), []);
    t.deepEqual(getSubsections(undefined, [ 'a' ]), []);
});

test('does not return the current section', (t) => {
    t.plan(1);
    t.deepEqual(getSubsections('a', [ 'a' ]), []);
});

test('does not match partial substringed sections not separated by a dot', (t) => {
    t.plan(1);
    t.deepEqual(getSubsections('a', [ 'aa' ]), []);
});

test('matches only direct descendants', (t) => {
    t.plan(1);
    const allReferences = [
        'a.b.c',
        'a.q',
        'a.n'
    ];
    t.deepEqual(getSubsections('a', allReferences), [ 'a.q', 'a.n' ]);
});

test('matches all valid child sections', (t) => {
    t.plan(1);
    const allReferences = [
        'a',
        'b',
        'a.b.c',
        'awasd.asd',
        'a.q',
        'a.b'
    ];
    t.deepEqual(getSubsections('a', allReferences), [ 'a.q', 'a.b' ]);
});
