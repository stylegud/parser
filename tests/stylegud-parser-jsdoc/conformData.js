import test from 'ava';
import path from 'path';
import { conformData } from 'stylegud-parser-jsdoc';

test('it should properly parse JSON data', (t) => {
    t.plan(2);
    const inputs = [
        '{"a": "a"}',
        `{
            "test2": "wow"
        }`
    ];
    const expectations = [
        {
            name: '',
            description: '',
            datum: { a: 'a' }
        },
        {
            name: '',
            description: '',
            datum: { 'test2': 'wow' }
        }
    ];

    inputs.forEach((item, key) => t.deepEqual(conformData(item), expectations[key]));
});

test('it should fail for invalid input', (t) => {
    const inputs = [
        '{a:\'a\'}',
        ''
    ];
    t.plan(inputs.length);
    inputs.forEach((input) => {
        t.throws(() => conformData(input));
    });
});

test('it should allow data tags without description', (t) => {
    t.plan(1);
    const expected = {
        name: '',
        description: '',
        datum: { a: 'a' }
    };
    t.deepEqual(conformData('{"a": "a"}'), expected);
});

test('it should parse out a name from the first line', (t) => {
    t.plan(1);

    const expected = {
        name: 'a name',
        description: '',
        datum: { a: 'a' }
    };
    const input = `a name
        {"a": "a"}`;
    t.deepEqual(conformData(input), expected);
});

test('it should parse out a description from the 2nd line', (t) => {
    const tests = [
        [
            `a name

            a description
            {"a": "a"}`,
            {
                name: 'a name',
                description: 'a description',
                datum: { a: 'a' }
            }
        ],
        [
            `a name
            a multiline
            description
            {"a": "a"}`,
            {
                name: 'a name',
                description: 'a multiline description',
                datum: { a: 'a' }
            }
        ]
    ];
    t.plan(tests.length);

    tests.forEach(([ input, expected ]) => {
        t.deepEqual(conformData(input), expected);
    });
});

test('it should load a path from a file', (t) => {
    const srcObj = {
        meta: {
            path: path.resolve(__dirname, '../fixtures/')
        }
    };
    const tests = [
        [
            './testData.json',
            [
                {
                    name: 'From JSON',
                    description: 'wow from JSON',
                    datum: {
                        a: 'b'
                    }
                }
            ],
            './testData.js',
            [
                {
                    name: 'hello from JS',
                    description: 'wow from JS',
                    datum: {
                        a: 'b'
                    }
                },
                {
                    name: 'hello2',
                    description: 'wooooah',
                    datum: {
                        a: 'b'
                    }
                }
            ]
        ]
    ];
    t.plan(tests.length);

    tests.forEach(([ input, expected ]) => {
        t.deepEqual(conformData(input, srcObj), expected);
    });
});
