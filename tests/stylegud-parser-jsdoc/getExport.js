import test from 'ava';
import { getExport } from 'stylegud-parser-jsdoc';

test('it should get export from a meta code name', (t) => {
    t.plan(1);
    const section = { meta: { code: { name: 'exports.poop' } } };
    t.is(getExport(section), 'poop');
});

test('it should get export from first export tag text', (t) => {
    t.plan(1);
    const section = {
        tags: {
            export: [ {
                originalTitle: '',
                title: '',
                text: 'woohoo',
                value: ''
            } ]
        }
    };
    t.is(getExport(section), 'woohoo');
});

test('it should get export from first exports tag text', (t) => {
    t.plan(1);
    const section = {
        tags: {
            exports: [ {
                originalTitle: '',
                title: '',
                text: 'whee',
                value: ''
            } ]
        }
    };
    t.is(getExport(section), 'whee');
});

test('it should get export from a meta code name over export(s) tag', (t) => {
    t.plan(1);
    const section = {
        meta: { code: { name: 'exports.wat' } },
        tags: {
            export: [ {
                originalTitle: '',
                title: '',
                text: 'woohoo',
                value: ''
            } ],
            exports: [ {
                originalTitle: '',
                title: '',
                text: '!!!',
                value: ''
            } ]
        }
    };
    t.is(getExport(section), 'wat');
});

test('it should get export from export tag over exports tag', (t) => {
    t.plan(1);
    const section = {
        tags: {
            export: [ {
                originalTitle: '',
                title: '',
                text: 'wow',
                value: ''
            } ],
            exports: [ {
                originalTitle: '',
                title: '',
                text: 'woohoo',
                value: ''
            } ]
        }
    };
    t.is(getExport(section), 'wow');
});
