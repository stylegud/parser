import test from 'ava';
import path from 'path';
import { parser as Parser } from 'stylegud-parser-jsdoc';

test('it should load data from files', (t) => {
    const options = {
        src: path.resolve(__dirname, '../fixtures/jsDoc.js')
    };
    const expectedData = [
        {
            name: 'hello from JS',
            description: 'wow from JS',
            datum: {
                a: 'b'
            }
        },
        {
            name: 'hello2',
            description: 'wooooah',
            datum: {
                a: 'b'
            }
        },
        {
            name: 'From JSON',
            description: 'wow from JSON',
            datum: {
                a: 'b'
            }
        }
    ];
    return new Parser(options)
        .parse()
        .then((results) => {
            t.deepEqual(results[0].source.data, expectedData);
        });
});
