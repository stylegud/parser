import test from 'ava';
import { swapKeys } from 'stylegud-object-mapper';


test('it should swap strings', (t) => {
    t.plan(1);
    const input = {
        targetKey: 'source-key',
        delicious: '美味しい'
    };
    const expected = {
        'source-key': 'targetKey',
        '美味しい': 'delicious'
    };
    t.deepEqual(swapKeys(input), expected);
});

test('it should swap objects', (t) => {
    t.plan(1);
    const transform = () => {};
    const input = {
        targetKey: {
            key: 'source-key',
            transform
        },
        delicious: {
            key: '美味しい',
            transform
        }
    };
    const expected = {
        'source-key': {
            key: 'targetKey',
            transform
        },
        '美味しい': {
            key: 'delicious',
            transform
        }
    };
    t.deepEqual(swapKeys(input), expected);
});

test('it should merge identical keys', (t) => {
    t.plan(1);
    const transformA = () => {};
    const transformB = () => {};
    const input = {
        targetKey: {
            key: 'source-key',
            transformA
        },
        delicious: {
            key: 'source-key',
            transformB
        }
    };
    const expected = {
        'source-key': [
            {
                key: 'targetKey',
                transformA
            },
            {
                key: 'delicious',
                transformB
            }
        ]
    };
    t.deepEqual(swapKeys(input), expected);
});

test('it should ignore empty values', (t) => {
    t.plan(1);
    const input = {
        targetKey: '',
        delicious: undefined,
        wheee: null
    };
    const expected = {};
    t.deepEqual(swapKeys(input), expected);
});

test('it should throw on Arrays', (t) => {
    t.plan(1);
    t.throws(() => swapKeys({ a: [] }));
});

test('it should throw on missing key prop for objects', (t) => {
    t.plan(1);
    const transform = () => {};
    const input = {
        targetKey: {
            transform
        }
    };
    t.throws(() => swapKeys(input));
});
