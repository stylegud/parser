import test from 'ava';
import { sgObjectMapper } from 'stylegud-object-mapper';


test('it should return valid data', (t) => {
    t.plan(1);
    const source = {
        a: 'b',
        c: {
            d: 'e'
        },
        f: 'g',
        r: 'n'
    };
    const map = {
        'a.b.c': 'a',
        'd': 'c.d',
        'a.b.f': 'f',
        'g': { key: 'r', template: (k) => k },
        'h': { key: 'r', template: (k) => k }
    };
    const result = sgObjectMapper(source, map);
    const expected = {
        a: { b: { c: 'b', f: 'g' } },
        d: 'e',
        g: 'n',
        h: 'n'
    };
    t.deepEqual(result, expected);
});

test('it should add all keys to target', (t) => {
    t.plan(1);
    const source = { a: 'q' };
    const map = {
        'a.b.c': undefined,
        'd': undefined,
        'a.b.f': undefined
    };
    const result = sgObjectMapper(source, map);
    const expected = {
        a: { b: { c: undefined, f: undefined } },
        d: undefined
    };
    t.deepEqual(result, expected);
});
