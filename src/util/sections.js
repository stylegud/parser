const { strToPath, pathToStr, isImmediateChild } = require('./references');

/**
 * Given a list of existing sections sets a `sections`
 * property on the section explicitly listing its subsections
 *
 * @export
 * @param {Object[]} [sections=[]] A list of all sections
 * @return {Object[]} The mutated sections
 */
function setSections(sections = []) {
    const references = sections.map((section) => section.reference);
    sections.forEach((section) => {
        section.sections = getSubsections(section.reference, references);
    });
    return sections;
}

/**
 * Given a baseReference and a list of references, returns all references
 * that are contained within the baseReference
 *
 * @export
 * @param {String} [baseReference=''] The main reference that is a parent
 * @param {Object[]} [references=[]] The entire list of all references in the document
 * @return {String[]} An array containning the references of sections that are immediate children of this section
 */
function getSubsections(baseReference = '', references = []) {
    const basePath = strToPath(baseReference);
    return references.filter((ref) => ref && baseReference !== ref)
        .map(strToPath)
        .filter((path) => isImmediateChild(basePath, path))
        .map(pathToStr);
}

module.exports = { setSections, getSubsections };
