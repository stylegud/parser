const fs = require('fs');

/**
 * Attempts to load a config from string, package.json
 * or passed in object
 *
 * @param {String|Object} options The options to use
 * @returns {Object} The loaded package
 * @throws
 */
function getOptions(options) {
    // Try to load a string
    if (typeof options === 'string') {
        return require(options);
    }
    // Look in package.json
    if (options === undefined) {
        if (process.env.npm_package_stylegud) {
            return process.env.npm_package_stylegud;
        }
        throw Error('No configuration specified');
    }
    // Return what's left
    return options;
}

/**
 * Creates a directory if it does not already exist
 *
 * @link https://stackoverflow.com/questions/21194934/node-how-to-create-a-directory-if-doesnt-exist
 * @param {String} path The path to be created
 * @param {String} mask The permissions for the path
 * @param {Function} cb Callback once complete
 * @returns {Promise} Resolves when directory has been created
 */
function ensureDirExists(path, mask) {
    if (typeof mask === 'function') { // allow the `mask` parameter to be optional
        mask = 0o777;
    }
    return new Promise((resolve, reject) => {
        fs.mkdir(path, mask, function(err) {
            if (err) {
                if (err.code === 'EEXIST') {
                    resolve(); // ignore the error if the folder already exists
                }
                else {
                    reject(err); // something else went wrong
                }
            }
            else {
                resolve(); // successfully created folder
            }
        });
    });
}

module.exports = { getOptions, ensureDirExists };
