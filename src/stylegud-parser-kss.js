const pathModule = require('path');
const kss = require('kss');
const { sgObjectMapper } = require('./stylegud-object-mapper');


/**
 * A parser for KSS data, usually found in CSS
 *
 * @param {Object} options The options passed by a user for this parser
 * @param {String|String[]} options.src An string or array of string paths for
 * files to parse, may be globs
 */
function KSSParser(options) {
    this.name = 'KSSParser';
    this.src = options.src;
}

/**
 * Parses files for KSS into an object
 *
 * @returns {Promise} A promise resolving to an object of parsed data
 */
KSSParser.prototype.parse = function() {
    return kss({
        source: this.src,
        markdown: false,
        json: true
    })
    .then((data) => data.sections)
    .then((sections) => sections.map(conformKss));
};

const propMap = {
    'header': 'header',
    'description': 'description',
    'deprecated': 'deprecated',
    'experimental': 'experimental',
    'reference': 'reference',
    'markup': {
        key: 'markup',
        transform: (v) => (v ? [ v ] : []) // eslint-disable-line
    },
    'source.filename': {
        key: 'source.filename',
        transform: (v) => pathModule.basename(v)
    },
    'source.path': {
        key: 'source.path',
        transform: (v) => pathModule.dirname(v)
    },
    'source.line': 'source.line',
    'source.data': 'modifiers',
    'source.data[].name': 'modifiers[].name',
    'source.data[].description': 'modifiers[].description',
    'source.data[].datum': 'modifiers[].className',
    'source.parameters': 'parameters[]',
    'renderer': 'renderer'
};

/**
 * Conforms an object to fit expected values for Stylegud section data
 *
 * @param {Object} section The non-conformant section
 * @returns {Object} The conformed section
 */
function conformKss(section) {
    return sgObjectMapper(section, propMap);
}

module.exports = {
    parser: KSSParser,
    conformKss
};
