const jsdoc = require('jsdoc-api');
const fs = require('fs');
const path = require('path');
const R = require('ramda');
const { sgObjectMapper } = require('./stylegud-object-mapper');


/**
 * A parser for reading JSDoc and outputting properly formatted objects
 *
 * @param {Object} options The options for the parser
 * @constructor {JSDocParser} JSDocParser
 */
function JSDocParser(options) {
    this.name = 'JSDocParser';
    this.src = options.src;
}

JSDocParser.prototype.parse = function() {
    return jsdoc.explain({
        template: 'jsdoc-json',
        files: this.src
    })
    .then((data) => data.filter(filterJSDoc))
    .then((data) => data.map(conformJSDoc));
};

/**
 * Ensures that only items with an @styleguide tag get included
 *
 * @param {Object} item The particular jsdoc comment to filter in/out
 * @returns {Boolean} Whether to include it or not
 */
function filterJSDoc(item = {}) {
    if (!item.tags) {
        return false;
    }
    return item.tags.find((tag) => tag.title === 'styleguide');
}


/**
 * Converts a list of tags to an object with properties=tag.title
 *
 * @param {Object[]} tags An array of tags
* @returns {Object} An object keyed by tag title
 */
function conformTags(tags = []) {
    return tags.reduce((acc, tag) => {
        acc[tag.title] = acc[tag.title] || [];
        acc[tag.title].push(tag);
        return acc;
    }, {});
}


/**
 * Converts a JSDoc object into an object that matches the expected format
 * for the styleguide accumulator
 *
 * @param {Object} section The JSDoc section to be conformed
 * @returns {Object} The conformed object
 */
function conformJSDoc(section) {
    section = R.clone(section);
    section.tags = conformTags(section.tags);
    const data = sgObjectMapper(section, propMap);
    data.source.export = getExport(section);
    return data;
}

/**
 * Conforms JSDoc meta output to match expected stylegud "source" output
 *
 * @param {Object} section The JSDoc data
 * @returns {Object} The conformed "source" object
 */
function conformMeta(section) {
    const meta = section.meta;
    return {
        filename: meta.filename,
        path: meta.path,
        line: meta.lineno,
        export: getExport(section)
    };
}

const EXPORTS_MATCHER = /^exports\./;

/**
 * Fixes the name of an export: export.blah becomes blah
 *
 * @param {Object} section The section
 * @param {Object[]} tags Tags to check
 * @returns {String} The modified export value
 */
function getExport(section = {}) {
    const tags = section.tags || {};
    const name = R.pathOr('', 'meta.code.name'.split('.'), section);
    if (name.match(EXPORTS_MATCHER)) {
        return name.replace(EXPORTS_MATCHER, '');
    }
    const exportTag = (tags.export || tags.exports || [])[0];
    if (exportTag) {
        if (exportTag.text) {
            return exportTag.text;
        }
        return section.name;
    }
    return '';
}

/**
 * Converts data tags into data objects, splits out the description and datum
 *
 * Expects { text: 'StringObject|StringFilePath' }
 *
 * @param {Object[]} text The text to parse
 * @param {Object[]} srcObject The parent source object
 * @returns {Object[]} The conformed data
 */
function conformData(text, srcObject) {
    text = text.trim();
    const lines = text.split(/[\r\n]+/).filter(Boolean).map(trim);

    const objStartIdx = lines.findIndex(isProbablyObject);

    if (lines.length === 1 && objStartIdx === -1) {
        const ext = path.extname(text);
        const filePath = path.resolve(path.join(srcObject.meta.path, text));

        switch (ext) {
            case '.json':
                return JSON.parse(fs.readFileSync(filePath));
            case '.js':
                return require(filePath);
            default:
                throw new Error('Could not parse data, invalid file type encountered for', text);
        }
    }
    else if (objStartIdx === -1) {
        throw new Error('Could not parse data: ', text);
    }

    const name = objStartIdx > 0 ? lines[0] : '';
    const description = objStartIdx > 1 ? lines.slice(1, objStartIdx).join(' ') : '';
    const jsonToParse = lines.slice(objStartIdx).join(' ');
    let datum;
    try {
        datum = JSON.parse(jsonToParse);
    }
    catch (e) {
        console.error('Could not parse invalid JSON: ', jsonToParse);
        throw e;
    }

    return {
        name,
        description,
        datum
    };
}

/**
 * Tests if a string is probably an object
 * @param {String} text Some text to test
 * @returns {Boolean} Whether the text is probably an object or not
 */
function isProbablyObject(text) {
    return text.length > 0 && text[0] === '{';
}

const trim = (s) => s.trim();

module.exports = {
    parser: JSDocParser,
    conformJSDoc,
    conformTags,
    conformMeta,
    getExport,
    conformData
};

const propOrArray = R.propOr([]);
const mapToProp = (p) => R.map(R.prop(p));
const mapArrayKeyToProp = (key, prop) => R.compose(mapToProp(prop), propOrArray(key));
const mapDataToText = mapArrayKeyToProp('data', 'text');
const conformDataFlat = (src) => R.chain(R.partialRight(conformData, [ src ]));
const transformDataArray = (item, src) => R.compose(conformDataFlat(src), mapDataToText)(item);

const propMap = {
    'header': 'name',
    'description': 'description',
    'deprecated': 'deprecated',
    'experimental': 'experimental',
    'reference': {
        key: 'tags',
        transform: (t) => R.propOr([ {} ], 'styleguide', t)[0].value
    },
    'markup': {
        key: 'tags',
        transform: mapArrayKeyToProp('markup', 'value')
    },
    'source.filename': 'meta.filename',
    'source.path': 'meta.path',
    'source.line': 'meta.lineno',
    'source.data': {
        key: 'tags',
        transform: transformDataArray
    },
    'renderer': 'renderer'
};
