#!/usr/bin/env node
const path = require('path');
const program = require('commander');
const pkg = require('../package.json');
const StylegudParser = require('./stylegud-parser');

let confFile;
program.version(pkg.version)
    .arguments('[conf]')
    .action((cmd) => {
        confFile = path.resolve(process.cwd(), cmd);
    })
    .parse(process.argv);

const parser = new StylegudParser(confFile);
parser.run().then((data) => {
    if (parser.dest === 'stdout') {
        if (typeof data !== 'string') {
            data = JSON.stringify(data);
        }
        console.log(data);
    }
});
